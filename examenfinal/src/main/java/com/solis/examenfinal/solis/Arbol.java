/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solis.examenfinal.solis;

/**
 *
 * @author pc
 */
public class Arbol {

    /**
     * @param args the command line arguments
     */
   public static void main(String[] args) {
        int dato;
        String nombre;
        
        //JOSUE SOLIS mi matricula es 63831 por lo tanto es inOrden
        //para este caso JOSUE SOLIS MAY = JOSUELIMAY 
        ArbolBinario miArbol = new ArbolBinario();
        miArbol.agregarNodo(74, "J");
        miArbol.agregarNodo(79, "O");
        miArbol.agregarNodo(83, "S");
        miArbol.agregarNodo(85, "U");
        miArbol.agregarNodo(69, "E");
        miArbol.agregarNodo(76, "L");
        miArbol.agregarNodo(73, "I");
        miArbol.agregarNodo(77, "M");
        miArbol.agregarNodo(65, "A");
        miArbol.agregarNodo(89, "Y");

        System.out.println("inOrden");
        if (!miArbol.estaVacio()){
            miArbol.inOrden(miArbol.raiz);
        }
        
        
    }
    
}