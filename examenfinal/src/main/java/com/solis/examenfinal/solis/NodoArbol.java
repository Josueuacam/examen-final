/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.solis.examenfinal.solis;

/**
 *
 * @author pc
 */
public class NodoArbol {
    int dato;
    String nombre;
    NodoArbol hijoizquierdo, hijoderecho;
    public NodoArbol (int d, String s){
        this.dato = d;
        this.nombre = s;
        this.hijoizquierdo = null;
        this.hijoderecho = null;
    }
    public String toString(){
        return nombre + "El dato es : "+dato;
    }
}
